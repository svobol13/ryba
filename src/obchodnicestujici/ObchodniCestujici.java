package obchodnicestujici;

import java.io.*;
import java.util.*;

/**
 *
 * @author zimmel
 */
public class ObchodniCestujici {

    private static Map<String, Integer> routeCosts = new HashMap<String, Integer>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        initRouteCosts();
        List<int[]> permutations = new ArrayList<int[]>();
        int[] numbersToVisit = new int[] {1,2,3,4,5,6,7,8,9};
        for (int i = 0; i < numbersToVisit.length; i++) {
            permute(new int[] {numbersToVisit[i]}, excludeIndexFromArray(numbersToVisit, i), permutations);
        }
        routeCosts = evaluateRoutes(permutations);
        for(String key : routeCosts.keySet()) {
            System.out.println(key + " has price: " + routeCosts.get(key));
        }
    }

    private static Map<String, Integer> evaluateRoutes(List<int[]> routes) {
        Map<String, Integer> result = new HashMap<String, Integer>();
        for (int[] route : routes) {
            int totalCost = calculateTotalCost(route);
            if (totalCost >= 0) {
                result.put(Arrays.toString(route), totalCost);
            }
        }
        return result;
    }
    
    /**
     * Initialize routeCosts map.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void initRouteCosts() throws FileNotFoundException, IOException {
        File file = new File("C:\\Developer\\trasy.txt");
        Scanner sc = new Scanner(file);
        String line;
        // skip first line
        sc.next();
        while (sc.hasNextLine()) {
            // read new line
            line = (sc.next() + "");
            // remove ; endline char
            line = line.substring(0, line.length() - 1);
            // spliting by comma
            String[] split = line.split(",");
            // make a map key x,y
            String key = split[0] + "," + split[1];
            int cost = Integer.parseInt(split[2]);
            routeCosts.put(key, cost);
        }
    }

    /**
     * Returns route cost from to or -1 if route does not exist.
     *
     * @param from
     * @param to
     * @return cost
     */
    private static int getRouteCost(int from, int to) {
        if (from == to) {
            return 0;
        }
        String key = from > to ? to + "," + from : from + "," + to;
        // attempts to retrieve cost from map
        Integer cost = routeCosts.get(key);
        // returns cost or negative number
        return cost != null ? cost : -1;
    }

    /**
     * Evaluate total price of route. If |1,3| = 5 and |3,2| = 1 then for
     * {1,3,2} returns 6
     *
     * @param route
     * @return totalCost
     */
    private static int calculateTotalCost(int[] route) {
        int total = 0;
        for (int i = 0; i < route.length - 1; i++) {
            int point2pointCost = getRouteCost(route[i], route[i + 1]);
            // route does not exists
            if (point2pointCost < 0) {
                return -1;
            }
            total += point2pointCost;
        }
        return total;
    }


    public static void printPermutations(int[] n, int[] Nr, int idx) {
        if (idx == n.length) {  //stop condition for the recursion [base clause]
            System.out.println(Arrays.toString(n));
            return;
        }
        for (int i = 0; i <= Nr[idx]; i++) {
            n[idx] = i;
            printPermutations(n, Nr, idx + 1); //recursive invokation, for next elements
        }
    }
    
    
    private static void permute(int[] prefix, int[] remaining, List<int[]> permutationList) {
        int[] result = new int[prefix.length + remaining.length];
        
        if (remaining.length == 1) {
            permutationList.add(joinArrays(prefix, remaining));
//            System.out.println(Arrays.toString(joinArrays(prefix, remaining)));
            return;
        }
        
        for (int i = 0; i < remaining.length; i++) {
            int[] current = joinArrays(prefix, new int[] {remaining[i]});
            permute(current, excludeIndexFromArray(remaining, i), permutationList);
        }
    }
    
    private static int[] joinArrays(int[] begin, int[] end) {
        int[] result = new int[begin.length + end.length];
        for (int i = 0; i < begin.length; i++) {
            result[i] = begin[i];
        }
        for (int i = 0; i < end.length; i++) {
            result[i+begin.length] = end[i];
        }
        return result;
    }
    
    private static int[] excludeIndexFromArray(int[] input, int indexToExclude) {
        int[] result = new int[input.length - 1];
        int mod = 0;
        for (int i = 0; i < input.length; i++) {
            if (i == indexToExclude) {
                mod = -1;
                continue;
            }
            result[i + mod] = input[i];
        }
        return result;
    }
}
